const { FoodCategoryController } = require("../controllers")
const route = require("express").Router()
const { authentication } = require("../middleware")


route.get("/", FoodCategoryController.getAllFoodCategory)
route.get("/:id", FoodCategoryController.getOneFoodCategory)

route.use(authentication)

route.post("/", FoodCategoryController.createFoodCategory)
route.put("/:id", FoodCategoryController.updateFoodCategory)
route.delete("/:id", FoodCategoryController.deleteFoodCategory)


module.exports = route