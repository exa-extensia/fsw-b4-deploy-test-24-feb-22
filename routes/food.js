const { FoodController } = require("../controllers");
const route = require("express").Router();

route.get("/", FoodController.getAllFood)
route.get("/:id", FoodController.getOneFood)
route.post("/", FoodController.createFood)
route.put("/:id", FoodController.updateFood)
route.delete("/:id", FoodController.deleteFood)

module.exports = route