const route = require("express").Router()
const routeUser = require("./user")
const routeFood = require("./food")
const routeCategory = require("./category")
const routeFoodCategory = require("./foodcategory")

route.use("/user", routeUser)
route.use("/food", routeFood)
route.use("/category", routeCategory)
route.use("/foodcategory", routeFoodCategory)

route.use("/", routeFood)

module.exports = route