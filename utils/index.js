const encrypt = require("./encrypt");
const token = require("./token");

module.exports = {
  encrypt,
  token
}

