'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Foods extends Model {
    static associate(models) {
      Foods.belongsTo(models.Users)
      Foods.belongsToMany(models.Category, { through: models.FoodCategory, foreignKey: 'FoodsId'})

    }
  }
  Foods.init({
    id: {
      primaryKey: true,
      type: DataTypes.STRING
    },
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    UserId: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Foods',
  });
  return Foods;
};