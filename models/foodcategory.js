'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class FoodCategory extends Model {
    static associate(models) {
      models.Foods.belongsToMany(models.Category, { through: models.FoodCategory, foreignKey: 'FoodsId' })
      models.Category.belongsToMany(models.Foods, { through: models.FoodCategory, foreignKey: 'CategoryId' })
    }
  }
  FoodCategory.init({
    FoodsId: DataTypes.STRING,
    CategoryId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'FoodCategory',
  });
  return FoodCategory;
};