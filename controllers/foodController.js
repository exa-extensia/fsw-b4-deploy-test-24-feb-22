const { Foods, Category } = require("../models");
const uuid = require("uuid");

class FoodController{
  static async getAllFood(req, res){
    try {
      const options = {
        include: [
          {
            model: Category,
            attributes: ["name", "type"]
          }
        ]
      }
      const data = await Foods.findAll(options)

      res.status(200).json({ data })
    } catch (error) {
      console.log(error)
    }
  }

  static async getOneFood(req, res){
    const id = req.params.id
    const options = {
      where: {
        id
      },
      include: [
        {
          model: Category,
          attributes: ["name", "type"]
        }
      ]
    }

    const data = await Foods.findOne(options)
    
    res.status(200).json({ data })
  }

  static async createFood(req, res){
    const id = uuid.v4()
    const { name, description, UserId } = req.body
    const payload = {
      id, name, description, UserId
    }

    const newFood = await Foods.create(payload)

    res.status(200).json({ data: newFood })
  }

  static async updateFood(req, res){
    const { name, description, userId } = req.body

    const payload = {
      name, description, userId
    }

    const id = req.params.id
    const options = {
      where: {
        id
      },
      returning: true
    }

    const updatedFood = await Foods.update(payload, options)

    res.status(200).json({ data: updatedFood })
  }

  static async deleteFood(req, res){
    const id = req.params.id
    const deletedFood = await Foods.destroy({
      where: {
        id
      },
      returning: true
    })

    res.status(200).json({ data: deletedFood })
  }
}

module.exports = FoodController