const { Foods, Category } = require("../models")

class CategoryController{
  static async getAllCategory(req, res){
    try {
      const options = {
        include: [
          {
            model: Foods
          }
        ]
      }

      const data = await Category.findAll(options)
      res.status(200).json({ data })
    } catch (error) {
      console.log(error)
    }
  }

  static async getOneCategory(req, res){
    const id = req.params.id
    const options = {
      where: {
        id
      }
    }

    const data = await Category.findOne(options)
  }

  static async createCategory(req, res){
    const { name, type } = req.body
    const payload = {
      name, type
    }

    const newCategory = await Category.create(payload)
    res.status(200).json({ data: newCategory })
  }

  static async updateCategory(req, res){
    const { name, type } = req.body
    const payload = {
      name, type
    }

    const id = req.params.id
    const options = {
      where: {
        id
      }
    }

    const updatedCategory = await Category.update(payload, options)
    res.status(200).json({ data: updatedCategory })
  }

  static async deleteCategory(req, res){
    const id = req.params.id
    const deletedCategory = await Category.destroy({
      where: {
        id
      },
      returning: true
    })

    res.status(200).json({ data: deletedCategory })
  }
}

module.exports = CategoryController